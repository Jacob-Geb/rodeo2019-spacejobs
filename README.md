### spaceJOBS

## Requirments

```
Node v10.16.3 LTS (npm v6.9.0)
https://nodejs.org/dist/v10.16.3/node-v10.16.3.pkg
```

## Setup

```
$ git clone <repo>
$ cd repo
$ npm install
```

## Commands

```
$ npm start // watches for changes, and starts dev server localhost:8080
$ npm run build:debug
$ npm run build:release

```