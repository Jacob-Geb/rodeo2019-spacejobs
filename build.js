
try {
    console.log('Adding buildInfo to project.');

    const fs = require('fs')

    const env = process.argv.slice(2)[0];
    // console.log('env: ' + env);

    const package = fs.readFileSync('package.json');
    const packageJson = JSON.parse(package);
    const version = packageJson.version;
    // console.log('version: '+version);

    let aestTime = new Date().toLocaleString("en-AU", {timeZone: "Australia/Brisbane"});
    aestTime = new Date(aestTime);
    aestTime = aestTime.toLocaleString();
    // console.log('AEST time: '+aestTime);

    let nyTime = new Date().toLocaleString("en-US", {timeZone: "America/New_York"});
    nyTime = new Date(nyTime);
    nyTime = nyTime.toLocaleString();
    // console.log('USA time: '+nyTime);

    const rev = require('child_process')
        .execSync('git rev-parse HEAD')
        .toString().trim()

    const shortRev =  rev.substr(0, 7);
    // console.log('revision: '+shortRev);

    const buildInfo = {
        /*auto generated - edits will be overwritten!*/
        'env':env,
        'version':version,
        'aestTime':aestTime,
        'nyTime':nyTime,
        'shortRev':shortRev
    };

    var jsonString = JSON.stringify(buildInfo);
    fs.writeFile("static/assets/config/buildInfo.json", jsonString, 'utf8', function (err) {
        if (err) {
            console.err("An error occured while writing buildInfo to File.");
            return console.err(err);
        }
    
        // console.log("BuildInfo file has been saved.");
    });

}
catch (e) {
    console.error('Failed to generate build info :(');
}