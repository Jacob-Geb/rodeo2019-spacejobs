import * as PIXI from 'pixi.js';
import { ScaleManager } from 'springroll';
import { ResizeService, } from './services';
import { Constants } from './config/Constants';
import { GameScene } from './scens/GameScene';
import { PlanetPool } from './pools/PlanetPool';
import { PlayerPool } from './pools/PlayerPool';
import { GameUI } from './objects/GameUI';
import { GameState } from './GameState';
import { GameInput } from './services/GameInput';
import { Player } from './objects/Player';

export class Game {

    public PixiApp: PIXI.Application;
    private highScore: number;

    private planetPool: PlanetPool;
    private playerPool: PlayerPool;

    private gameInput: GameInput;
    private gameState: GameState;
    private gameScene: GameScene;
    private gameUI: GameUI;

    constructor() {

        this.PixiApp = new PIXI.Application(Constants.PixiAppOptions);
        document.getElementById('content').appendChild(this.PixiApp.view);
        this.PixiApp.renderer.backgroundColor = 0xFFFFFF;
        this.PixiApp.ticker.add(this.update); 

        const resizeService = new ResizeService(this.PixiApp.renderer, this.PixiApp.stage);
        const scaleManager = new ScaleManager(resizeService.ResizeGame);
        resizeService.TriggerResize();

        this.initGame();
    }

    private initGame = (): void => {

        this.planetPool = new PlanetPool();
        this.playerPool = new PlayerPool();
        this.gameInput = new GameInput();

        this.gameScene = new GameScene(this.planetPool, this.playerPool);
        this.PixiApp.stage.addChild(this.gameScene);

        this.gameUI = new GameUI(this.playerPool);
        this.PixiApp.stage.addChild(this.gameUI);

        this.gameScene.on('score', this.onScore);
        this.gameScene.on('playerDied', this.onPlayerDeath);
        this.gameInput.on('keyPressed', this.onKeyPressed);

        this.prepareGame();
    }

    private update = (dt: number): void => {
        let deltaInSeconds = PIXI.ticker.shared.elapsedMS / 1000;
        this.gameScene.Update(dt);
        this.gameUI.Update(dt);
    }

    private prepareGame = (): void => {

        console.log('prepareGame');
        if (this.gameState != GameState.WaitingOnPlayers){
            this.gameState = GameState.WaitingOnPlayers;

            this.planetPool.Reset();
            this.playerPool.Reset();
            this.gameScene.PrepareGame();
            
            this.gameUI.ShowWaitingOnPlayers();
            this.gameUI.once('playersReady', this.startGame)
        }
    }

    private startGame = (): void => {

        console.log('startGame');
        if (this.gameState != GameState.InProgress){
            this.gameState = GameState.InProgress;

            this.gameScene.StartNewGame();
            this,this.gameUI.OnGameStart();
        }
    }

    private onKeyPressed = (key: string): void => {

        if (this.gameState == GameState.WaitingOnPlayers){
            this.tryRegisterPlayer(key);
        }
        else if (this.gameState == GameState.InProgress){

            let player = this.playerPool.GetPlayerByKey(key);
            if (player && player.IsAlive){
                player.TryJump(); 
            }
        }
    }

    private tryRegisterPlayer = (key: string): void => {

        if (this.playerPool.GetPlayerByKey(key)){
            console.log('old player: small jump');
        }
        else{

            if (this.playerPool.PlayersCount < this.playerPool.MaxPlayers){

                console.log('new player: '+key);
                let earth = this.planetPool.Planets[0];
                let player = this.playerPool.assignPlayer(key, earth);
    
                this.gameUI.RegisterPlayer(player);
                this.gameScene.RegisterPlayer(player);
    
                // console.log('new player: '+key);
                // this.playerPool.assignPlayer(key, this.startingPlanet);
            }
        }
    }

    private onScore = (player: Player, value: number, yVal: number): void => {
        player.Score += value;
        this.gameUI.OnScoreUpdate(player);

        if (value == Constants.PointBig){
            this.gameUI.PlanetFlash(player.Color, yVal);
        }
    }

    private onPlayerDeath = (player: Player): void => {

        console.log('onPlayerDeath');

        this.gameUI.OnPlayerDeath(player);// todo 

        if(this.playerPool.everybodyIsDead()) {
            this.onGameOver()
        }
    }

    private onGameOver = (): void => {

        console.log('onGameOver');

        if (this.gameState != GameState.Over){
            this.gameState = GameState.Over;

            this.gameScene.StopGame();
            this.gameUI.OnGameOver();
            this.gameUI.once('restartReady', this.prepareGame);
        }
    }

    // private showStartScreen = () => {

    //     this.planetPool.Reset();
    //     this.playerPool.Reset();
    //     this.planetPool.GeneratePlanets(Constants.Width / 2, Constants.Width * 2);
    //     const initialPlanet = this.planetPool.Planets[0];

    //     this.startScene = new StartScene(this.playerPool, initialPlanet);
    //     this.PixiApp.stage.addChild(this.startScene);
    //     this.startScene.once('startGame', this.InitGame);
    // }



    // private InitGame = (): void => {
    //     if(this.startScene) {
    //         this.PixiApp.stage.removeChild(this.startScene);
    //         this.startScene = null;
    //     }
    //     console.log("Init Game");
    //     this.gameScene = new GameScene(this.planetPool, this.playerPool);
    //     this.PixiApp.stage.addChild(this.gameScene);

    //     this.gameScene.on("gameOver", this.gameOver);
    // }

    // private gameOver = (): void  => {
    //     this.showStartScreen();
    // }
}