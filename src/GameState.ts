
export enum GameState {
    WaitingOnPlayers,
    InProgress,
    Over
}