
export class Constants {

    public static readonly Width: number = 1150;
    public static readonly Height: number = 720;

    public static readonly GameplayTop: number = 75;
    public static readonly GameplayBot: number = Constants.Height;

    public static readonly MinPlanetScale: number = 0.5;
    public static readonly MaxPlanetScale: number = 2;
    public static readonly MinPlanetRotationSpeed: number = 2;
    public static readonly MaxPlanetRotationSpeed: number = 4;

    public static readonly PointSmall: number = 1;
    public static readonly PointBig: number = 10;

    public static readonly PixiAppOptions: {} = {
        width : Constants.Width,
        Height : Constants.Height,
        // forceCanvas: true
    };
}