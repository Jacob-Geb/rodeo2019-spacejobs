import { Container } from "pixi.js";
import { Constants } from "../config/Constants";

export class Background extends Container{

    private cont1: Container;
    private cont2: Container;

    constructor () {
        super();

        this.y = Constants.Height/2;

        this.cont1 = new Container();
        this.addChild(this.cont1);

        let bg = PIXI.Sprite.fromImage('assets/images/spaceBG_0.png');
        bg.anchor.set(0, 0.5);
        this.cont1.addChild(bg);
        bg = PIXI.Sprite.fromImage('assets/images/spaceBG_0.png');
        bg.x = Constants.Width;
        bg.anchor.set(0, 0.5);
        this.cont1.addChild(bg);

        this.cont2 = new Container();
        this.cont2.alpha = 0.5;
        this.addChild(this.cont2);
        
        bg = PIXI.Sprite.fromImage('assets/images/spaceBG_1.png');
        bg.anchor.set(0, 0.5);
        this.cont2.addChild(bg);
        bg = PIXI.Sprite.fromImage('assets/images/spaceBG_1.png');
        bg.x = Constants.Width;
        bg.anchor.set(0, 0.5);
        this.cont2.addChild(bg);
    }

    public Update = (dt: number, speed: number):void => {
        this.cont1.x -= speed * dt * 0.3;
        if (this.cont1.x < -Constants.Width){
            this.cont1.x += Constants.Width;
        }

        this.cont2.x -= speed * dt * 0.55;
        if (this.cont2.x < -Constants.Width){
            this.cont2.x += Constants.Width;
        }
    }
}