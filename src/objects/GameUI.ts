import { Container, Point } from "pixi.js";
import { PlayerPool } from "../pools/PlayerPool";
import { Score } from "./Score";
import { Player } from "./Player";
import { TimerView } from "./TimerView";
import { ScoreView } from "./ScoreView";
import { Constants } from "../config/Constants";
import { JMath } from "../util/JMath";
import { PointFlash } from "./PointFlash";
import { DeathFlash } from "./DeathFlash";

export class GameUI extends Container{

    private timerView: TimerView;
    private scoreViews: ScoreView[];

    private playerPool: PlayerPool;
    private scores: Score[] = [];

    private timeTillStart: number;
    private readonly maxTimeTillStart: number = 2300;//4000

    private gameOver: boolean;
    private topText: PIXI.Text;
    private bottomText: PIXI.Text;
    private scoreCont: Container

    constructor (playerPool: PlayerPool) {
        super();

        this.playerPool = playerPool;

        this.scoreCont = new Container();
        this.addChild(this.scoreCont);
        this.scoreViews = [];

        this.timerView = new TimerView();
        this.addChild(this.timerView);

        for (let i = 0; i < playerPool.MaxPlayers; i++){
            let score = new ScoreView(i * 200, 5, playerPool.Colors[i]);
            this.scoreViews.push(score);
            this.scoreCont.addChild(score);
        }

        this.topText = new PIXI.Text("", {
                fontFamily : 'Lilita One', fontSize: 50,
                stroke: 0x000000,
                strokeThickness: 6,
                fill : 0xeae3b3, align : 'center'});

        this.topText.position.set(Constants.Width/2, 120);
        this.topText.anchor.set(0.5);
        this.addChild(this.topText);
    }

    public ShowWaitingOnPlayers = ():void => {

        this.gameOver = false;
        this.timeTillStart = this.maxTimeTillStart;
        this.timerView.UpdateTimer(this.timeTillStart/this.maxTimeTillStart);

        this.scoreViews.forEach(scoreView => {
            scoreView.Teardown();
        });
        
        this.topText.text = "Press a key to add a player!"; 
        this.topText.style.fill = 0xeae3b3;
        this.topText.visible = true;
    }

    public OnGameStart = ():void => {
        this.topText.visible = false;
    }

    public RegisterPlayer = (player: Player):void => {

        this.timeTillStart = this.maxTimeTillStart;
        this.timerView.UpdateTimer(this.timeTillStart/this.maxTimeTillStart);

        for (let i = 0; i < this.scoreViews.length; i++){
            if (!this.scoreViews[i].visible){
                this.scoreViews[i].Init(player);
                this.scoreCont.x = JMath.lerp(500, 100, (i/4));
                break;
            }
        }
    }

    public OnPlayerDeath = (player: Player):void => {

        for (let i = 0; i < this.scoreViews.length; i++){
            if (this.scoreViews[i].Player == player){
                this.scoreViews[i].OnDeath();

                let flash = new DeathFlash(player.y, player.Color);
                this.addChild(flash);
                return;
            }
        }
    }

    public OnGameOver = ():void => {

        this.gameOver = true;
        this.timeTillStart = this.maxTimeTillStart;
        this.timerView.UpdateTimer(this.timeTillStart/this.maxTimeTillStart);

        let winner = this.playerPool.Players[0]; 
        for (let i = 0; i < this.playerPool.Players.length; i++){
            if (winner.Score < this.playerPool.Players[i].Score){
                winner = this.playerPool.Players[i];
            }
        }

        this.topText.text = "Game over! "+ winner.Key +" Wins!"; 
        this.topText.style.fill = winner.Color;
        this.topText.visible = true;
    }

    public OnScoreUpdate = (player: Player):void => {

        for (let i = 0; i < this.scoreViews.length; i++){
            if (this.scoreViews[i].Player == player){
                this.scoreViews[i].SetScore(player.Score);
                break;
            }
        }
    }

    public PlanetFlash = (colour: number, yPos: number):void => {
        let flash  = new PointFlash(yPos, colour);
        this.addChild(flash);
    }

    public Update = (dt: number):void => {

        if (this.gameOver){

            this.timeTillStart -= (dt/60) * 1000;
            if (this.timeTillStart <= 0){
                this.emit('restartReady');
            }
            this.timerView.UpdateTimer(this.timeTillStart/this.maxTimeTillStart);
        }
        else {
            if (this.playerPool.PlayersCount > 0){

                this.timeTillStart -= (dt/60) * 1000;
                if (this.timeTillStart <= 0){
                    this.emit('playersReady');
                }
    
                this.timerView.UpdateTimer(this.timeTillStart/this.maxTimeTillStart);

                // this.scores.forEach(element => {
                //     element.Update(dt);
                // });
            }
        }
    }
}