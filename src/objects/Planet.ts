import { Container, Sprite, Point, Texture } from "pixi.js";
import { Rand } from "../util/Rand";
import { Player } from "./Player";
import { PlanetType } from "./PlanetType";
import { JMath } from "../util/JMath";

export class Planet extends Container{

    public Radius: number;
    public RotationSpeed: number;
    public IsDestroyed: boolean;
    public ClaimedPlayer: Player;
    public get IsBouncePlanet(): boolean {
        return this.planetType == PlanetType.Bounce;
    }

    private planetType: PlanetType;
    private colorMatrix: PIXI.filters.ColorMatrixFilter;
    private hueCount: number = 0;
    private planet: Sprite;

    constructor (pos: Point, scale: number, rotationSpeed: number, planetType: PlanetType) {
        super();

        this.planetType = planetType;
        this.position.set(pos.x, pos.y);
        this.Radius = scale * 50;
        this.IsDestroyed = false;

        this.RotationSpeed = rotationSpeed;
        if (planetType == PlanetType.Bounce){
            this.RotationSpeed = 0;
        }

        this.initPlanetLook(scale);
    }

    private initPlanetLook = (scale: number):void => {

        if (Math.random() > 0.55){
            const bg = Sprite.fromImage('assets/images/planetBG_'+Rand.Any('0', '1')+'.png');
            bg.rotation = Math.random()*Math.PI * 2;
            bg.scale.set(scale * 0.5);
            bg.anchor.set(0.5);
            this.addChild(bg);
        }

        let texture: Texture;
        if (this.planetType == PlanetType.Earth){
            texture = Texture.fromImage('assets/images/planet_earth.png');
            this.planet = new Sprite(texture);
        }
        else if (this.planetType == PlanetType.Bounce){
            texture = Texture.fromImage('assets/images/planet_bounce.png');

            this.colorMatrix = new PIXI.filters.ColorMatrixFilter();
            this.hueCount = Math.random() * 360;

            this.planet = new Sprite(texture);
            this.planet.filters = [this.colorMatrix];
        }
        else {
            let id = Rand.Any('01', '02', '03');
            texture = Texture.fromImage('assets/images/planet_'+id+'.png');
            this.colorMatrix = new PIXI.filters.ColorMatrixFilter();
            this.colorMatrix.hue(JMath.Range(-180, 180));

            this.planet = new Sprite(texture);
            this.planet.filters = [this.colorMatrix];
        }

        this.planet.anchor.set(0.5);
        this.planet.scale.set(scale * 0.5);
        this.addChild(this.planet);

        if (this.planetType == PlanetType.Normal){
            this.planet.tint = 0xef9393;

            let colorMatrix = new PIXI.filters.ColorMatrixFilter();
            colorMatrix.hue(JMath.Range(-180, 180));
            this.planet.filters = [colorMatrix];
        }

        if (Math.random() > 0.75){
            const foreground = Sprite.fromImage('assets/images/foreground_'+Rand.Any('0', '1')+'.png');
            foreground.rotation = Math.random()*Math.PI * 2;
            foreground.scale.set(scale * 0.5);
            foreground.anchor.set(0.5);
            this.addChild(foreground);
        }
    }

    public Update = (dt: number, gameSpeed: number):void => {
        this.planet.rotation += (dt/60) * this.RotationSpeed;
        this.position.set(this.position.x - gameSpeed, this.position.y);

        if (this.planetType == PlanetType.Bounce){
            this.hueCount += dt * 2;
            this.colorMatrix.hue(this.hueCount);
            this.scale.set(1 + Math.sin(this.hueCount/15) * 0.05);
        }
    }

    public Destroy = ():void => {
        this.IsDestroyed = true;
        // this.IsDestroyed = this.position.x < -this.Radius;
    }

    public ClaimBy = (player: Player): void => {

        if (this.planetType == PlanetType.Normal){
            this.ClaimedPlayer = player;
            this.planet.filters = [];
            this.planet.tint = player.Color;
        }
    }
}