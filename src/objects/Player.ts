import { Container, Sprite, Point } from "pixi.js";
import { Planet } from "./Planet";
import { JMath } from "../util/JMath";
import { Constants } from "../config/Constants";
import { TimelineMax } from "gsap";

export class Player extends Container{

    public Radius: number;
    public Color: number;
    public Key: string;
    public JumpDirection: Point = new Point();
    public GravIgnore: number = 0;
    public IsAlive: boolean = true;
    public Score: number = 0;
    public get IsJumping(): boolean {
        return this.planet == null;
    }

    private playerSprite: Sprite;
    private planet: Planet = null;// curent planet we're on
    private lastPlanet: Planet = null;
    private playerScale: number = 0.3;
    private jumpSpeed: number = 5;
    private s: number = 0;

    constructor (planet: Planet, color: number, key: string) {
        super();

        this.Radius = this.playerScale * 50;
        this.Color = color;
        this.Key = key;
        
        this.name = name;
        this.position = new Point(0, 0);

        this.playerSprite = PIXI.Sprite.fromImage('assets/images/100PicCircle.png');
        this.playerSprite.anchor.set(0.5);
        this.playerSprite.scale.set(this.playerScale);
        this.playerSprite.tint = color;
        this.addChild(this.playerSprite);
        
        let char = new PIXI.Text(this.Key);
        char.anchor.set(0.5);
        this.addChild(char);

        this.LandOnPlanet(planet);
        this.s = JMath.Range(0, 2 * Math.PI);
    }

    public TryJump = ():void => {

        if (this.planet){
            let dir = new Point();
            dir.x = this.x - this.planet.x;
            dir.y = this.y - this.planet.y;
            dir = JMath.normalize(dir);

            this.JumpDirection = dir;
            this.planet = null;

            this.position.x += this.JumpDirection.x;
            this.position.y += this.JumpDirection.y;
            this.GravIgnore = 0;
        }
    }

    public LandOnPlanet = (planet: Planet):void => {

        this.planet = planet;

        if (planet.IsBouncePlanet){
            this.TryJump();
        }
        else{

            this.planet = planet;
            this.lastPlanet = planet;
            planet.ClaimBy(this);
            this.s = Math.atan2(this.y -planet.y, this.x - planet.x);
            this.updateMovement(0);

            this.playerSprite.rotation = 0;
            this.playerSprite.scale.set(this.playerScale);
            this.JumpDirection = new Point();

            this.emit('planetClaimed', this, planet);
        }
    }

    public Update = (dt: number):void => {
        this.updateMovement(dt);

        if (this.JumpDirection.x == 0 && this.JumpDirection.y == 0){
            this.playerSprite.rotation = 0;
            this.playerSprite.scale.set(this.playerScale);
        }
        else{
            this.playerSprite.rotation = Math.atan2(this.JumpDirection.y, this.JumpDirection.x);
            this.playerSprite.scale.set(this.playerScale * 1.1, this.playerScale * 0.9);
        }
    }

    public Kill = () => {
        this.IsAlive = false;
        this.x = -100;
    }

    private updateMovement = (dt: number): void => {

        if (this.planet){
            this.s += this.planet.RotationSpeed * (dt/60);
            let r = this.planet.Radius + this.Radius;
            this.x = this.planet.x + r * Math.cos(this.s);
            this.y = this.planet.y + r * Math.sin(this.s);  
        }
        else{
            this.GravIgnore = Math.min(1, this.GravIgnore + dt * 0.005);

            // move in the direction we jumpped
            this.position.x += this.JumpDirection.x * this.jumpSpeed * dt;
            this.position.y += this.JumpDirection.y * this.jumpSpeed * dt;

            if (this.y < 60){
                this.JumpDirection.y = JMath.lerp(this.JumpDirection.y, 1, dt * 0.1);
            }
            else if (this.y > Constants.Height - 30){
                this.JumpDirection.y = JMath.lerp(this.JumpDirection.y, -1, dt * 0.1);
            }

            if (this.x > Constants.Width){
                this.JumpDirection.x = JMath.lerp(this.JumpDirection.x, -1, dt * 0.1);
            }
            
            this.JumpDirection = JMath.normalize(this.JumpDirection);
        }
    }
}