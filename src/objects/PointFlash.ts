
import { Container } from "pixi.js";
import { TimelineMax, Back, Power0 } from "gsap";

export class PointFlash extends Container{

    public Lifetime: number = 140;

    constructor (y: number, colour: number) {
        super();

        this.position.set(0, y);

        let flash = PIXI.Sprite.fromImage('assets/images/pointFlash.png');
        flash.anchor.set(0, 0.5);
        flash.scale.set(0)
        flash.tint = colour;
        this.addChild(flash);

        let tl = new TimelineMax();
        tl.to(flash.scale, 0.1, {x:1, y:1, ease: Back.easeOut.config(3)});
        tl.to(flash.scale, 0.1, {x:0, y:0, ease: Power0.easeInOut}, 0.1);
        tl.to(flash, 0.1, {alpha: 0, ease: Power0.easeInOut, onComplete: this.onComplete}, 0.1);
    }

    private onComplete = ():void => {
        this.parent.removeChild(this);
    }
}

