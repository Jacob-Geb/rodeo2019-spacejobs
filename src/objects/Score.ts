import { Container, Sprite, Point, Texture } from "pixi.js";
import { Constants } from "../config/Constants";
import { Rand } from "../util/Rand";
import { JMath } from "../util/JMath";
import { Player } from "./Player";

export class Score extends Container{

    private player: Player;
    private text: PIXI.Text;

    constructor (player: Player, point: Point) {
        super();

        this.player = player;
        console.log(player.Color);
        this.text = new PIXI.Text("" + player.Score, {fill : player.Color});
        this.position = point;

        this.addChild(this.text);
        
    }

    public Update = (dt: number):void => {
        this.text.text = "Score: " + this.player.Score;
    }
}