import { Container, Sprite } from "pixi.js";
import { Player } from "./Player";

export class ScoreView extends Container{

    public Player : Player;

    private playerKey: string = '';
    private scoreField: PIXI.Text;
    private readonly digits: number = 5;

    constructor (x: number, y: number, col: number) {
        super();
        this.position.set(x, y);
        this.scoreField = new PIXI.Text('', {fontFamily : 'Lilita One', fontSize: 40, fill : col, align : 'center'});
        this.addChild(this.scoreField);
        this.SetScore(0);
    }

    public Init = (player: Player): void => {
        this.Player = player;
        this.playerKey = player.Key;
        this.scoreField.alpha = 1;
        this.SetScore(0);
        this.visible = true;
    }

    public Teardown = (): void => {
        this.Player = null;
        this.visible = false;
    }

    public SetScore = (value: number): void => {
        this.scoreField.text = this.pad(value, this.digits);
    }

    public OnDeath = (): void => {
        this.scoreField.alpha = 0.5;
    }

    private pad = (num: number, size: number): string => {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        s += ' '+this.playerKey;
        return s;
    }
}