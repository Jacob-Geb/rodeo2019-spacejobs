import { Container, Sprite } from "pixi.js";
import { Constants } from "../config/Constants";

export class TimerView extends Container{

    private timerBG: Sprite;
    private timer: Sprite;
    private colorMatrix: PIXI.filters.ColorMatrixFilter;

    private readonly barScaleX: number = Constants.Width/2;

    constructor () {
        super();

        let topBar = Sprite.fromImage('assets/images/bg.png');
        topBar.scale.set(Constants.Width/2, 30);
        topBar.position.set(Constants.Width/2, 0);
        topBar.alpha = 0.3;
        topBar.tint = 0x000000;;
        topBar.anchor.set(0.5, 0);
        this.addChild(topBar);

        this.timerBG = Sprite.fromImage('assets/images/bg.png');
        this.timerBG.position.set(Constants.Width/2, 50);
        this.timerBG.scale.set(this.barScaleX, 5);
        this.timerBG.anchor.set(0.5, 0);
        this.timerBG.tint = 0x434154;
        this.addChild(this.timerBG);

        this.timer = Sprite.fromImage('assets/images/bg.png');
        this.timer.position.set(Constants.Width/2, 50);
        this.timer.scale.set(this.barScaleX, 5);
        this.timer.anchor.set(0.5, 0);
        this.timer.tint = 0x00ff00;
        this.addChild(this.timer);

        this.colorMatrix = new PIXI.filters.ColorMatrixFilter();
        this.colorMatrix.hue(0);
        this.timer.filters = [this.colorMatrix];
    }

    public UpdateTimer = (value: number):void => {
        this.timer.scale.x = this.barScaleX * Math.max(0, value);
        this.colorMatrix.hue((1 - value) * -150);
    }
}