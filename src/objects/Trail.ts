import { Container, Point } from "pixi.js";
import { Player } from "./Player";
import { JMath } from "../util/JMath";

export class Trail extends Container{

    public lifetime: number;
    public vel: Point;

    constructor (origin: Point, colour: number, vel: Point) {
        super();

        this.position = origin.clone();
        this.vel = vel;

        this.lifetime = JMath.IntRange(40, 130);
        this.scale.set(JMath.Range(0.7, 1.3));

        let trail = PIXI.Sprite.fromImage('assets/images/trail.png');
        trail.tint = colour;
        this.addChild(trail);
    }

    public Update = (dt: number, gameSpeed: number):void => {

        this.lifetime -= dt;
        if (this.lifetime < 0){
            this.parent.removeChild(this);
        }
        else{
            this.x += this.vel.x - gameSpeed;
            this.y += this.vel.y;
        }
    }
}

