import { Container } from "pixi.js";
import { Planet } from "../objects/Planet";
import { Constants } from "../config/Constants";
import { JMath } from "../util/JMath";
import { Point } from "pixi.js";
import { PlanetType } from "../objects/PlanetType";

export class PlanetPool {
    public Planets: Planet[] = [];
    public static bounceCount: number;

    public Reset = (): void => {
        this.Planets = [];
        PlanetPool.bounceCount = 10;
    }

    public GenerateEarth = (pos: Point, container: Container):void => {
        const earth = new Planet(pos, 1.4, 2.5, PlanetType.Earth);

        this.Planets.push(earth);
        container.addChild(earth);
    }

    public GeneratePlanets = (fromX: number, toX: number, container: Container):void => {
        console.log("Generating planets from ", fromX, toX);
        let currentLocation = fromX;
        
        while(currentLocation < toX)
        {
            const scale = JMath.Range(Constants.MinPlanetScale, Constants.MaxPlanetScale);
            const x = currentLocation;
            const y = JMath.Range(Constants.Height * 0.2, Constants.Height * 0.9);
            // const y = JMath.Range(0, 0.7 * Constants.Height) + 0.15 * Constants.Height;
      
            let rotationSpeed = JMath.Range(Constants.MinPlanetRotationSpeed, Constants.MaxPlanetRotationSpeed);
            rotationSpeed *= JMath.RandBool() ? 1 : -1;
            
            let planetType = PlanetType.Normal;
            PlanetPool.bounceCount --;
            if (PlanetPool.bounceCount <= 0){
                planetType = PlanetType.Bounce;
                PlanetPool.bounceCount = JMath.IntRange(5, 12);
            }

            const planet = new Planet(
                new Point(x, y), scale, 
                rotationSpeed, planetType
            );

            this.Planets.push(planet);
            container.addChild(planet);
            
            currentLocation += planet.Radius * 2 + 100;
        }

        // const planet = new Planet(
        //     "Planet" + (Math.random() * 100000000), 
        //     new Point(currentLocation, 0), 
        //     1, 
        //     50,
        //     false
        // );
        // this.Planets.push(planet);
        // if(container) {
        //     container.addChild(planet);
        // }
    }

    public RemoveDeadPlanets = (): void => {

        let newPlanets: Planet[] = [];

        this.Planets.forEach(planet => {

            if (planet.x > -300){
                newPlanets.push(planet);
            }
            else{
                planet.parent.removeChild(planet); 
                planet = null; 
            }
        });

        this.Planets = newPlanets;
    }
}