import { Player } from "../objects/Player";
import { Planet } from "../objects/Planet";

export class PlayerPool {

    public Players: Player[] = [];
    public readonly MaxPlayers: number = 5;
    public get PlayersCount(): number { return this.Players.length || 0; }
    public Colors: number[] = [0x1ced09, 0xefe304, 0xef2409, 0xef09d1, 0xe1eff2]
    
    private playersByKeycode:{ [index:string] : Player } = {}

    public Reset = (): void => {
        this.Players = [];
        this.playersByKeycode = {};
    }

    public GetPlayerByKey = (key: string): Player => {

        for (let i = 0; i < this.Players.length; i++){
            if (this.Players[i].Key == key){
                return this.Players[i];
            }
        }
        return null;
    }

    public assignPlayer (key: string, planetHandle: Planet): Player {
        let keyCode = key.charCodeAt(0);
        // if(keyCode >= 65 && keyCode <= 90 && this.playersByKeycode[keyCode])
        //     return;

        console.log("Registering player for key ", key, keyCode);

        if (this.Players.length < this.MaxPlayers) {
            let player = new Player( planetHandle, this.Colors[this.Players.length], key);
            this.Players.push(player);
            this.playersByKeycode[keyCode] = player;

            return player;
        }
    }

    public everybodyIsDead = () => {
        return this.Players.filter(p => !p.IsAlive).length == this.Players.length;
    }
}
