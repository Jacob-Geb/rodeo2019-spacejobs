import { Container, Point } from "pixi.js";
import { Constants } from "../config/Constants";
import { Planet } from "../objects/Planet";
import { Player } from "../objects/Player";
import { JMath } from "../util/JMath";
import { PlanetPool } from '../pools/PlanetPool';
import { Background } from "../objects/Background";
import { DeathThing } from "../objects/DeathThing";
import { PlayerPool } from "../pools/PlayerPool";
import { Trail } from "../objects/Trail";

export class GameScene extends Container{

    private planetPool: PlanetPool;
    private playerPool: PlayerPool;
   
    private readonly startSpeed: number = 2;
    private gameSpeed: number = 0;

    private distanceTravelled: number = 0;
    private bg: Background;
    private deathThing: DeathThing;
    private playerCont: Container;
    private planetCont: Container;
    private trailCont: Container;

    constructor (planetPool: PlanetPool, playerPool: PlayerPool) {
        super();

        this.planetPool = planetPool;
        this.playerPool = playerPool;

        this.bg = new Background();
        this.addChild(this.bg);

        // this.deathThing = new DeathThing();
        // this.addChild(this.deathThing);

        this.trailCont = new Container();
        this.addChild(this.trailCont);

        this.planetCont = new Container();
        this.addChild(this.planetCont);

        this.playerCont = new Container();
        this.addChild(this.playerCont);
    }

    public PrepareGame = ():void => {

        this.playerCont.removeChildren();
        this.planetCont.removeChildren();

        this.gameSpeed = 0;
        this.planetPool.GenerateEarth(new Point(Constants.Width * 0.4, Constants.Height/2), this.planetCont);
        this.planetPool.GeneratePlanets(Constants.Width * 0.66, Constants.Width * 2, this.planetCont);
    }
    
    public RegisterPlayer = (player: Player):void => {
        this.playerCont.addChild(player)
        player.on('planetClaimed', this.onPlanetClaimed);
    }

    private onPlanetClaimed = (player: Player, planet: Planet):void => {
        
        for (let i = 0; i < 30; i++){

            let pos = planet.position.clone();

            let vel = new Point(JMath.Range(-1, 1), JMath.Range(-1, 1));
            vel = JMath.normalize(vel);

            pos.x += vel.x * (planet.Radius);
            pos.y += vel.y * (planet.Radius);

            vel.x *= JMath.Range(1.9, 2.1);
            vel.y *= JMath.Range(1.9, 2.1);

            this.trailCont.addChild(new Trail(pos, player.Color, vel));
        }
    }

    public StartNewGame = ():void => {
        this.gameSpeed = this.startSpeed;
    }

    public StopGame = ():void => {
        this.gameSpeed = 0;
    }

    public Update = (dt: number):void => {

        if (this.gameSpeed > 0){

            this.distanceTravelled += this.gameSpeed;

            if (this.distanceTravelled % (Constants.Width / 2) <= (this.gameSpeed-1)){
                this.planetPool.GeneratePlanets(Constants.Width * 1.5, Constants.Width * 2, this.planetCont);
                this.planetPool.RemoveDeadPlanets();
            }
            
            if (this.distanceTravelled % (Constants.Width * 4) == 0){
                this.gameSpeed += 1;
            }
        }
   
        this.updatePlanets(dt);
        this.updatePlayers(dt);
        this.updateTrails(dt);
        this.bg.Update(dt, this.gameSpeed);
    }

    private updatePlanets = (dt: number):void => {

        this.planetPool.Planets.forEach(planet => {

            planet.Update(dt, this.gameSpeed);

            if (!planet.IsDestroyed && planet.x < -planet.Radius){
                planet.Destroy();

                let player = planet.ClaimedPlayer;
                if (player != null){
                    this.emit('score', player, Constants.PointBig, planet.y);
                }
            }
        });
    }

    private updatePlayers = (dt: number):void => {
       
        this.playerPool.Players.forEach(player => {

            if (player.IsAlive){
                player.Update(dt);
                this.applyPlanetGravity(player, dt);
                this.tryLandOnPlanets(player, dt);

                if (player.x < -10){
                    player.Kill(); 
                    this.emit('playerDied', player);
                }
            }
        });
    }

    private trailCount: number = 0;
    private updateTrails = (dt: number):void => {

        this.trailCount -= dt;
        if (this.trailCount < 0){
            this.trailCount = 8;

            this.playerPool.Players.forEach(player => {
                if (player.IsAlive){
                    this.trailCont.addChild(new Trail(player.position, player.Color, new Point()));
                }
            });
        }

        this.trailCont.children.forEach(trail => {
            (trail as Trail).Update(dt, this.gameSpeed);
        });
    }

    private applyPlanetGravity = (player: Player, dt: number):void => {

        if (player.IsJumping){

            let closestPlanet: Planet;
            let closestDist: number = 999999999999;

            this.planetPool.Planets.forEach(planet => {

                let maxGravDist = 300;
                let dist = JMath.dist(player, planet);

                if (dist < maxGravDist){
                    this.movePlayerTowardsPlanet(player, dist, planet, 0.1)

                    if (closestDist > dist ){
                        closestDist = dist;
                        closestPlanet = planet;
                    }
                }
            });

            if (closestPlanet){
                this.movePlayerTowardsPlanet(player, closestDist, closestPlanet, 0.2)
            }
        }
    }

    private movePlayerTowardsPlanet = (player: Player,dist: number, planet: Planet, mult: number):void => {
        let str = 1 - (dist/planet.y);
        str *= player.GravIgnore * mult;

        let directioTowardsPlanet = new Point(planet.x - player.x, planet.y - player.y);
        directioTowardsPlanet = JMath.normalize(directioTowardsPlanet);
        
        let newJumpDir = player.JumpDirection;
        newJumpDir.x = JMath.lerp(newJumpDir.x, directioTowardsPlanet.x, str);
        newJumpDir.y = JMath.lerp(newJumpDir.y, directioTowardsPlanet.y, str);

        player.JumpDirection = JMath.normalize(newJumpDir);
    }

    private tryLandOnPlanets = (player: Player, dt: number):void => {

        if (player.IsJumping){

            this.planetPool.Planets.forEach(planet => {
                
                let dist = JMath.dist(player, planet);
                if (dist < planet.Radius + player.Radius){
                    player.LandOnPlanet(planet);
                    this.emit('score', player, Constants.PointSmall);
                    return;
                }
            });
        }
    }
}