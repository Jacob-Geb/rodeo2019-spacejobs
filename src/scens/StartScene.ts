import { Container } from "pixi.js";

export class StartScene extends Container {
    // private playerPool: PlayerPool;
    // private timer: number;
    // private timerText: PIXI.Text;
    // private timerInterval: any;
    // private timerRect: PIXI.Graphics;
    // private counter: number
    // private bg: Background;
    // private countDownStarted: boolean;
    // private initialPlanet: Planet;

    // private static DomContentLoaded: boolean = false;

    // constructor(playerPool: PlayerPool, initialPlanet: Planet){
    //     super();
    //     this.initialPlanet = initialPlanet;

    //     this.bg = new Background();
    //     this.addChild(this.bg);
    //     this.addChild(initialPlanet); 

    //     this.playerPool = playerPool;
    //     this.playerPool.Players.forEach(element => {
    //         this.addChild(element);  
    //     });

    //     this.timer = 5;
    //     this.timerText = new PIXI.Text("Press any key to pick player",{fontFamily : 'Arial', fontSize: 50, fill : 0xffffff, align : 'center'});
    //     this.timerText.position.x = 640;
    //     this.timerText.position.y = 100;
    //     this.timerText.anchor.x = 0.5;
    //     this.timerText.anchor.y = 0.5;
    //     this.addChild(this.timerText);

    //     if(StartScene.DomContentLoaded) {
    //         document.addEventListener('keydown', this.keydownEvent);
    //     } else {
    //         document.addEventListener('DOMContentLoaded', this.addKeyDownEvent);
    //     }
    // }

    // private keydownEvent = (event: any): void => {

    //     if(!this.countDownStarted) {
    //         this.startCountDown();
    //     }
    //     if (this.playerPool.Players.length < this.playerPool.MaxPlayers){

    //         const key = event.key.toUpperCase();
    //         this.playerPool.assignPlayer(key, this.initialPlanet);
    //         clearInterval(this.timerInterval);
    //         this.timerRect.scale.x = 1.0; 
    //         this.counter = 5;
    //         this.timerText.text = this.counter.toString();
    //         if (this.playerPool.Players.length < this.playerPool.MaxPlayers){
    //             this.timerInterval = setInterval(() => {
    //                 this.timerText.text = this.counter.toString();
    //                 this.timerRect.scale.x = this.timerRect.scale.x -0.18;
    //                 this.counter--;
    //                 if (this.counter < 0 ) {
                        
    //                     //Start game when timer reaches 0	
    //                     this.startGame();
    //                 }}, 1000);
    //         } else {
    //             this.startGame();
    //         }
    //     }
    // }

    // private addKeyDownEvent = (): void => {
    //     'use strict';
    //     StartScene.DomContentLoaded = true;
    //     document.addEventListener('keydown', this.keydownEvent);
    // }

    // private startGame = () => {
    //     clearInterval(this.timerInterval);
    //     this.removeChild(this.timerText);
    //     this.removeChild(this.timerRect);

    //     document.removeEventListener('keydown', this.keydownEvent);
    //     document.removeEventListener('DOMContentLoaded', this.addKeyDownEvent);

    //     this.emit('startGame');
    // }

    // private startCountDown = () => {
    //     this.timerRect = new PIXI.Graphics();
    //     this.timerRect.beginFill(0xff1010).drawRect(0, 0, 500, 50).endFill();
    //     this.timerRect.position.set(this.timerText.position.x - 200, this.timerText.position.y - 25);
    //     this.addChild(this.timerRect);
    //     this.addChild(this.timerText);
    //     let counter = 5;
    //     this.timerInterval = setInterval(() => {
    //         this.timerText.text = counter.toString();
    //         this.timerRect.scale.x = this.timerRect.scale.x -0.18;
    //         counter--;
    //         if (counter < 0 || this.playerPool.Players.length >= this.playerPool.MaxPlayers) {
                
    //             //Start game when timer reaches 0	
    //             this.startGame();	
    //         }
    //     }, 1000);
    // }

    // public Update = (dt: number): void => {
    //     this.playerPool.Players.forEach(player => {
    //         player.Update(dt);
    //     });
    // }
}