import { EventEmitter } from "events";

export class GameInput extends EventEmitter{

    constructor() {
        super();

        document.addEventListener('keydown', (event: any) => {

            if (event.keyCode >= 65 && event.keyCode <= 90 ){
                const key = event.key.toUpperCase();
                this.emit('keyPressed', key);
            }
        });
    }
}
