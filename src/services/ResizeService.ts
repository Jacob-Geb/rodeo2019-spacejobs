import { Constants } from "../config/Constants";
import { JMath } from "../util/JMath";
import { Point, Container } from "pixi.js";
import { PubSub } from "../util";

export class ResizeService {

    public static OnResize: PubSub = new PubSub();
    public static get GameScale(): number { return ResizeService.gameScale; }
    public static get Offset(): number { return ResizeService.offset; }
    private static gameScale: number = 1;
    private static offset: number = 0;

    private renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
    private pixiCanvas: HTMLCanvasElement;
    private stage: Container;

    private WidthHeightRatio: number;
    private MinWidthHeightRatio: number;

    private marg: Point = new Point();

    constructor(renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer, stage: Container) {

        this.renderer = renderer;
        this.stage = stage;
        this.pixiCanvas = renderer.view;
        this.WidthHeightRatio = Constants.Width / Constants.Height;
    }

    public TriggerResize() {
        let evt = window.document.createEvent('UIEvents');
        (evt as any).initUIEvent('resize', true, false, window, 0);
        window.dispatchEvent(evt);
    }

    public ResizeGame = (evt: ResizeEvent): void => {

        let scale = 1;
        this.marg.x = this.marg.y = 0;

        if (evt.width / evt.height >= this.WidthHeightRatio) {// wider than max width
            scale = evt.height / Constants.Height;
            this.marg.x = (evt.width - (Constants.Width * scale)) / 2;
            ResizeService.offset = 0;
        }
        else {// narrower than safe zone
            scale = evt.width / Constants.Width;
            this.marg.x = -(Constants.Width - Constants.Width) * scale / 2;
            this.marg.y = (evt.height - (Constants.Height * scale)) / 2;
            ResizeService.offset = (Constants.Width - Constants.Width)/2;
        }

        this.renderer.resize(Math.floor(scale * Constants.Width), Math.floor(scale * Constants.Height));

        this.pixiCanvas.setAttribute('style', `
            transform-origin: top left;
            margin-left: ${this.marg.x}px; margin-top: ${this.marg.y}px;`
        );

        if (this.stage) {
            this.stage.scale.x = scale;
            this.stage.scale.y = scale;
        }

        ResizeService.gameScale = scale;
        ResizeService.OnResize.Dispatch();
    }
}

class ResizeEvent {
    public width: number;
    public height: number;
    public ratio: number;
}
