import { Sprite } from "pixi.js";

export class Button extends Sprite{

    private DEFAULT: number = 0;
    private MOUSE_DOWN: number = 1;
    private MOUSE_OVER: number = 2;

    public isMouseOver = false;
    private segments: number = 4;
    private horizontalLayout: boolean = true;

    private btnTextures: PIXI.Texture[];

    constructor(textureName: string, x: number = 0, y: number = 0, segments: number = 3, horizontalLayout: boolean = false) {
        super(null);

        this.x = x;
        this.y = y;
        this.segments = segments;
        this.horizontalLayout = horizontalLayout;
        this.anchor.set(0.5);

        this.setTexture(textureName);

        this.on("pointerdown", this.onButtonDown);
        this.on("pointerup", this.onButtonUp);
        this.on("pointerupoutside", this.onButtonUpOutSide);
        this.on("pointerover", this.onButtonOver);
        this.on("pointerout", this.onButtonOut);

        this.interactive = true;
        this.buttonMode = true;

        this.on("pointertap", () => {
            // PIXI.sound.Sound.from('assets/sounds/sfx/generic_click.mp3').play();
            // PIXI.
        });
    }

    private onButtonUp = () => {
        this.setSegment(this.DEFAULT);
    };

    private onButtonUpOutSide = () => {
        this.setSegment(this.DEFAULT);
    }

    private onButtonDown = () => {
        this.setSegment(this.MOUSE_DOWN);
        this.emit("buttondown");
    };

    private onButtonOut = () => {
        this.setSegment(this.DEFAULT);
        this.isMouseOver = false;
    };

    private onButtonOver = () => {
        this.setSegment(this.MOUSE_OVER);
        this.isMouseOver = true;
    };

    private setSegment = (id: number) => {
        this.texture = this.btnTextures[id];
    };

    public setTexture = (textureName: string) => {

        this.texture = PIXI.Texture.fromImage(textureName)

        this.btnTextures = [];
        let tileWidth, tileHeight;

        if (this.horizontalLayout){
            tileWidth = this.texture.width /this.segments;
            tileHeight = this.texture.height;

            for (let i = 0; i < 3; i++) {
                let tex = new PIXI.Texture(
                    this.texture as any,
                    new PIXI.Rectangle(i * tileWidth, 0, tileWidth, tileHeight)
                );
                this.btnTextures.push(tex);
            }
        }
        else {
            tileWidth = this.texture.width;
            tileHeight = this.texture.height/this.segments;
            
            for (let i = 0; i < 3; i++) {
                let tex = new PIXI.Texture(
                    this.texture as any,
                    new PIXI.Rectangle(0, i * tileHeight, tileWidth, tileHeight)
                );
                this.btnTextures.push(tex);
            }
        }

        this.setSegment(this.DEFAULT);
    };

    public resetState() {
        this.setSegment(this.DEFAULT);
    }
}