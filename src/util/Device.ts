export class Device
{
	public static get IsMobile(): boolean
    {
        return (("ontouchstart" in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
    }

    public static get IsDesktop(): boolean {
        return !this.IsMobile;
    }

    public static get lowEndDevice(): boolean {

        try {
            if (Device.isIOS){
                if(window.screen.height / window.screen.width == 1024 / 768) {
                    if (window.devicePixelRatio == 1) {
                        return true;// iPad, iPad 2, iPad Mini
                    }
                }
            }

            // Ipad mini, iPad 1-3
            // if (Device.isIOS && Device.majorIOSVersion < ){
            //     return true;
            // }

            return Device.isAndroid && Device.majorAndroidVersion < 5;
        }catch (e){}     
        
        return false;
        
        //Device.isTouchDevice;
    }

    public static get isAndroid(): boolean {
        let ua = ((window as any).ua || navigator.userAgent).toLowerCase(); 
        return ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    }

    public static get androidVersion(): string {
        let ua = ((window as any).ua || navigator.userAgent).toLowerCase(); 
        let match = ua.match(/android\s([0-9\.]*)/);
        return match ? match[1] : -1;
    }

    public static get majorAndroidVersion(): number {
        return parseInt(Device.androidVersion, 10);
    }

    public static get isIOS(): boolean {
        return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    }
}

// public static get isCordova(): boolean {
//     let app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
//     if ( app ) {
//         return true; // PhoneGap application
//     } 
//     return false;

//     // if (window.parent){
//     //     window.parent.hasOwnProperty("cordova") || window.parent.location.search.indexOf("cordova=true") >= 0;
//     // }
//     // return window.hasOwnProperty("cordova") || window.location.search.indexOf("cordova=true") >= 0;
// }
// public static get isIE11(): boolean {
//     return !!(window as any).MSInputMethodContext && !!(document as any).documentMode;
// }

// public static get isPixel3(): boolean {
//     let ua = (ua || navigator.userAgent).toLowerCase(); 
//     return ua.indexOf("pixel 3") > -1;
// }

// public static get majorIOSVersion(): number {
//     if (/iP(hone|od|ad)/.test(navigator.platform)) {
//         let v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
//         return parseInt(v[1], 10);
//     }
//     return -1;
// }
