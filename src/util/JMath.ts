import { Rectangle, Point } from "pixi.js";

export class JMath
{

	public static lerp(start: number, end: number, value: number): number {
        return start + (end - start) * JMath.clamp01(value);
    }

    public static inverseLerp(start: number, end: number, value: number): number {
        return (value - start) / (end - start);
    }

    public static lerpPoint(start: Point, end: Point, value: number): Point {
        return new Point(
            JMath.lerp(start.x, end.x, value),
            JMath.lerp(start.y, end.y, value));
    }
    
    public static clamp = (val:number, min:number, max:number): number => val < min ? min : val > max ? max : val;

    public static clamp01 = (val:number) => {
        return JMath.clamp(val, 0, 1);
    }

    public static clampPoint = (p:Point, bounds:Rectangle) : Point => {
        return new Point(
            JMath.clamp(p.x, bounds.x, bounds.x + bounds.width),
            JMath.clamp(p.y, bounds.y, bounds.y + bounds.height)
        )
    }

    public static smoothStep = (x: number) : number => {
        return -2 * Math.pow(x, 3) + 3 * Math.pow(x, 2);
    }

    public static Rand = (value: number = 1.0) : number => {
        return Math.random() * value;
    }

    public static RandBool = (): boolean => {
        return Math.random() > 0.5;
    }

    public static Range = (min: number, max: number) : number => {
        return Math.random() * (max - min) + min;
    }

    public static IntRange = (min: number, max: number) : number => {
        return Math.floor(JMath.Range(min, max));
    }

    public static dist = (a:IPoint, b:IPoint): number => {
        return Math.sqrt(JMath.srqDist(a, b));
    }

    public static srqDist = (a:IPoint, b:IPoint): number => {
        return Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2);
    }

    public static magnitude = (p: Point): number => {
        return JMath.dist(p, {x:0, y:0});
        // vs  sqrt(this.x*this.x + this.y*this.y);
    }

    public static normalize = (p: Point): Point => {
        let m = JMath.magnitude(p);
        return new Point(p.x/m, p.y/m);
    }

    public static moveTowards = (current:Point, target:Point, maxDistanceDelta: number): Point => {

        let vector = new Point(target.x - current.x, target.y - current.y);
        let magnitude = JMath.magnitude(vector);

        if (magnitude <= maxDistanceDelta || magnitude == 0){
            return target;
        }
        else{

            return new Point(
                current.x + vector.x*(maxDistanceDelta / magnitude),
                current.y + vector.y*(maxDistanceDelta / magnitude)
            );
        }
    }

    public static moveTowardsValue = (current: number, target: number, maxDistanceDelta: number): number => {
        if (target > current){

            let val = current + maxDistanceDelta;
            if (val > target){
                return target;
            }
            return val;
        }
        else{
            let val = current - maxDistanceDelta;
            if (val < target){
                return target;
            }
            return val;
        }
    }

    public static rotate(radius: number, radians: number): Point {
        if(radians == 0){
            return new Point(radius, 0);
        }
        else{
            return new Point((Math.cos(radians) * radius), (Math.sin(radians) * radius));
        }
     }
}

interface IPoint{
    x:number,
    y:number
}
