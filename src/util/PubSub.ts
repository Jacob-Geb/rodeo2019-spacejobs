export class PubSub{

    private methods: any[] = [];

    public Sub = (x : (a:any, b:any) => any) : void => {
        this.methods.push(x);
    }

    public Dispatch = (a: any = null, b: any= null): void => {
        for (let i = 0; i < this.methods.length; i++){
            this.methods[i](a, b);
        }
    }
}