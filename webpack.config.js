const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlConfig = require(path.join(__dirname, 'html.config'));

const ROOT = path.resolve( __dirname, "src" );
const deploy = path.join(__dirname, 'deploy');

module.exports = env => {
  const plugins = [
    new HtmlWebpackPlugin(HtmlConfig),
    new MiniCssExtractPlugin(),
    new CopyPlugin([{ from: path.join(__dirname + '/static'), to: deploy }])
  ];
  return {
    stats: 'errors-only',

    mode: env.dev ? 'development' : 'production',

    devServer: {
      contentBase: path.join(__dirname, '/static')
    },

    entry: "./src/index.ts",
    devtool: 'inline-source-map',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'deploy')
    },

    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    },
    resolve: {
        extensions: ['.ts', '.js' ],
        modules: [ ROOT, "node_modules" ]
    },
    plugins,
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader']
        },
        {
            test: /\.ts$/,
            use: 'ts-loader',
            exclude: /node_modules/
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        {
          test: /\.(otf|woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                useRelativePath: true
              }
            }
          ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: path.join(deploy + 'assets/image')
              }
            }
          ]
        },
        {
          test: /\.(mp3|ogg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: path.join(deploy + '/assets/audio/')
              }
            }
          ]
        },
        {
          test: /\.(mp4)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: path.join(deploy + '/assets/video/')
              }
            }
          ]
        }
      ]
    }
  };
};
